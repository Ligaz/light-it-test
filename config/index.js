var nconf = require('nconf');
var path = require('path');

//Подключем nconf для хранения всех настроек в одном json файле
nconf.argv()
    .env()
    .file({file: path.join(__dirname,'properties.json')});

module.exports = nconf;

