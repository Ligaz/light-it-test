var Sequelize = require('sequelize');
var sequelize = require('../sequelize');


var messageAtributes = {
    text: {
        type: Sequelize.TEXT('longtext'),
            allowNull: false
    },
    parentID:{
        type: Sequelize.INTEGER,
            allowNull: true
    },
    author:{
        type: Sequelize.TEXT('longtext'),
            allowNull:false
    },
    checked:{
        type: Sequelize.BOOLEAN,
        allowNull:false
    }

};

var messageOptions = {
    freezeTableName: true,
    timestamps: true
};

//Cоздаем модель для ORM
var Message = sequelize.define('messages',messageAtributes,messageOptions);

sequelize.sync();

module.exports.Message = Message;