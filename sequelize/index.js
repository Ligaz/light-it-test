var Sequelize = require('sequelize');
var path = require('path');
var config = require(path.resolve('config'));

//Инициализируем новый объект Sequelize для работы с БД
var sequelize = new Sequelize(config.get('database:name'),config.get('database:user'),config.get('database:password'), {
    host: 'localhost',
    dialect:'mysql',
    pool: {
        max: 100,
        min: 0,
        idle: 10000
    }
});

module.exports = sequelize;