'use strict'
var express = require('express'),
    app = express(),
    path = require('path'),
    router = require('./router/index'),
    api = require('./router/api'),
    auth = require('./router/auth'),
    socketIO = require('socket.io'),
    bodyParser = require('body-parser'),
    config = require(path.resolve('config')),
    cookieParser = require('cookie-parser'),
    passport = require('passport'),
    passwordHash = require('password-hash'),
    pug = require('pug'),
    http = require('http'),
    server = http.createServer(app),
    io = socketIO.listen(server),
    SocketHandler = require('./router/socketHandler')(io),
    VkStrategy = require('./passport/vk'),
    FacebookStrategy = require('./passport/facebook'),
    GoogleStrategy = require('./passport/google');

//Настраиваем bodyPareser, даем доступ к статическим файлам, задаем тип файлов для рендера и подключаем сессии
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/dist',express.static(path.join(__dirname,"dist")));
app.set('views', __dirname);
app.set('view engine', 'pug');
app.use(require('express-session')({
    secret: 'super secret',
    resave: false,
    saveUninitialized: false
}));
app.use(cookieParser());

//Блок настройки аутентификации пользователей
app.use(passport.initialize());
app.use(passport.session());
passport.use('vk',VkStrategy);
passport.use('facebook',FacebookStrategy);
passport.use('google',GoogleStrategy);
passport.serializeUser(function (user, done) {
    done(null, JSON.stringify(user));
});
passport.deserializeUser(function (data, done) {
    try {
        done(null, JSON.parse(data));
    } catch (e) {
        done(err)
    }
});

//Подключем сокеты и задаем роуты для приложения
app.use(SocketHandler);
app.use("/api",api);
app.use("/auth",auth);
app.use("/",router);

//Обработчик для некорректных запросов
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//Обработчик ошибок
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    console.log(err);
    res.render('views/error',err);
});

//Стартуем сервер
server.listen(config.get("connection:port"),function () {
        console.log('Application listening on port ' + config.get("connection:port"));
});

