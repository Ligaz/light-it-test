var AuthGoogleStrategy = require('passport-google-oauth20').Strategy,
    path = require('path'),
    config = require(path.resolve('config'));

var Strategy = new AuthGoogleStrategy({
        clientID: config.get("auth:google:id"),
        clientSecret: config.get("auth:google:key"),
        callbackURL: "http://"+config.get("connection:host")+":"+config.get("connection:port")+"/auth/google/callback"
    },
    function(accessToken, refreshToken, profile, done) {
            return done(null, {
                id: profile.id,
                username: profile._json.displayName,
                photoUrl: profile.photos[0].value,
                profileUrl: profile._json.url,
                provider:profile.provider
            });
    }
);

module.exports = Strategy;
