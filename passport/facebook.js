var AuthFacebookStrategy = require('passport-facebook').Strategy,
    path = require('path'),
    config = require(path.resolve('config'));

var Strategy = new AuthFacebookStrategy({
        clientID: config.get("auth:facebook:id"),
        clientSecret: config.get("auth:facebook:key"),
        callbackURL: "http://"+config.get("connection:host")+":"+config.get("connection:port")+"/auth/facebook/callback",
        profileFields: [
            'id',
            'displayName',
            'profileUrl',
            "gender",
            "photos"
        ]
    },
    function (accessToken, refreshToken, profile, done) {
        return done(null, {
            id: profile.id,
            username: profile.displayName,
            photoUrl: profile.photos[0].value,
            profileUrl: profile.profileUrl,
            provider:profile.provider

        });
    }
);

module.exports = Strategy;
