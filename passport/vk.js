var AuthVKStrategy = require('passport-vkontakte').Strategy,
    path = require('path'),
    config = require(path.resolve('config'));

var Strategy = new AuthVKStrategy({
        clientID: config.get("auth:vk:id"),
        clientSecret: config.get("auth:vk:key"),
        callbackURL: "http://"+config.get("connection:host")+":"+config.get("connection:port")+"/auth/vk/callback"
    },
    function (accessToken, refreshToken, profile, done) {
        return done(null, {
            id: profile.id,
            username: profile.displayName,
            photoUrl: profile.photos[0].value,
            profileUrl: profile.profileUrl,
            provider:profile.provider
        });
    }
);

module.exports = Strategy;
