var directives = angular.module('directives',[]);

//Директива для передачи пользователя при рендере страницы
directives.directive('ngData',function ($rootScope) {
    return{
        restrict: 'A',
        link: function(scope,element,attrs){
            $rootScope.user = JSON.parse(element[0].defaultValue);
        }
    }
});

//Диреткива для прослушивания событий скрола
directives.directive('infScroll', function() {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            var visibleHeight = element.height();
            var threshold = 100;
            element.scroll(function() {
                var scrollableHeight = element.prop('scrollHeight');
                var hiddenContentHeight = scrollableHeight - visibleHeight;
                if (hiddenContentHeight - element.scrollTop() <= threshold&&scope.isMore) {
                    scope.$apply(attrs.infScroll);
                }
            });
        }
    };
});
