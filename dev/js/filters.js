var filters = angular.module('filters',[]);

//Простой фильтр для обрезки string до нужной длинны с троеточием в конце
filters.filter('cropString',function(){
    return function (input,index){
        if(input){
            if(index>=input.length){
                return input;
            }else {
                return input.substring(0,index)+"...";
            }
        }else {
            return input;
        }


    }
});
