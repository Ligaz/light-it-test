var services = angular.module('services',[]);

//Сервис-обертка для объекта сокета
services.factory('socket', function ($rootScope) {
    var socket = io.connect();
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        },
        off: function (eventName, callback) {
            socket.off(eventName,function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket,args);
                })
            });
        }
    };
});


//Сервис для работы с сообщениями
services.factory('messageService',function ($rootScope,$http) {
    return {
        getFirstMessages: function () {
            $http.get('api/getFirstMessages')
                .then(
                    function (res) {
                       $rootScope.messageList = res.data.list;
                       $rootScope.messageCount = res.data.count;
                       if($rootScope.messageList.length<$rootScope.messageCount){
                           $rootScope.isMore = true;
                       }else {
                           $rootScope.isMore = false;
                       }
                    },
                    function (err) {
                        alert('Не удалось получить сообщения');
                    }
                )
        },
        loadMessages: function () {
            $rootScope.isMore = false;
            $http.get("api/load/" +$rootScope.page)
                .then(
                    function (res) {
                        angular.forEach(res.data.list,function(item){
                            $rootScope.messageList.push(item);
                        });
                        $rootScope.page++;
                        if($rootScope.messageList.length<$rootScope.messageCount){
                            $rootScope.isMore = true;
                        }else {
                            $rootScope.isMore = false;
                        }

                    },
                    function(err){
                        alert('Не удается загрузить сообщения');
                    }
                );
        },
        getChildMessages: function (id) {
            return $http.get('api/childMessages/'+id);
        },
        allMessages: function () {
            $http.get('api/allMessages')
                .then(
                    function (res) {
                        var allMessages = res.data.list;
                        angular.forEach(allMessages,function (item) {
                           var exist = _.findWhere($rootScope.messageList,{id:item.id});
                           if(!exist){
                               $rootScope.messageList.push(item);
                           }
                        });
                        $rootScope.isMore = false;
                    }
                )
        }
    }
})
