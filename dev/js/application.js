var app = angular.module('application',['ngMaterial','ctrls','directives','ngRoute', 'route-segment', 'view-segment','services','filters','ngAnimate']);

app.run(function ($rootScope, $location,messageService) {
    //При старте приложения задаем первую страницу для запросов infinite scroll и получем первые 10 сообщений
    $rootScope.page = 1;
    messageService.getFirstMessages();
});

