angular.module('application')
    .config(function ($locationProvider) {
        //Убираем хештеги при роутинге
        $locationProvider.html5Mode(true);
    });
