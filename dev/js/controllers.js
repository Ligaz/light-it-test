var controllers = angular.module('ctrls', ['services', 'filters']);

controllers.controller('SignCtrl', function ($scope, $window) {
    //Функция аутентификации
    $scope.auth = function (selector) {
        switch (selector) {
            case 'vk':
                $window.location.href = '/auth/vk';
                break;
            case 'facebook':
                $window.location.href = '/auth/facebook';
                break;
            case 'google':
                $window.location.href = '/auth/google';
                break;
        }
    }
});
controllers.controller('MainCtrl', function ($scope, $http, $window, socket, messageService) {

    //Добавляем слушателей для приема и обновления сообщений главной ветки
    socket.on('new message', function (result) {
        if ($scope.messageList) {
            $scope.messageList.unshift(result);
        }
    });

    socket.on('update message', function (result) {
        console.log(result);
        var oldMessage = _.findWhere($scope.messageList, {id: result.id});
        var index = $scope.messageList.indexOf(oldMessage);
        if (oldMessage.nodes) {
            result.nodes = oldMessage.nodes;
        }
        $scope.messageList.splice(index, 1, result);
    });

    //Функция выхода из системы
    $scope.logout = function () {
        $http.get('/api/logout')
            .then(
                function (res) {
                    if (res.data == 'logout') {
                        $window.location.href = '/';
                    }
                }
            )
    };

    //Переход на страницу авторизации
    $scope.getSignPage = function () {
        $window.location.href='/sign_in';
    };

    //Обработчик кнопки 'показать все'
    $scope.getAllMessages = function () {
      messageService.allMessages();
    };

    //Функция подгрузки сообщений (вызывается из директивы)
    $scope.loadData = function () {
        messageService.loadMessages();
    };

    //Отправка сообщения
    $scope.send = function (message) {
        var data ={};
        $scope.messageSended = true;//показываем пользователю значек загрузки
        if($scope.editMessage){//если сообщение обновляется
            data = {message: message, editID: $scope.editMessage.id};
            socket.emit('rewrite',data,function (result) {
                if (!result) {
                    alert('Ваше сообщение не было изменено')
                } else {
                    $scope.messageSended = false;
                    $scope.message = null;
                }
            });
        }else {//для новых сообщений
            data = {user: $scope.user, message: message, parentID:$scope.commentsMessage?$scope.commentsMessage.id:null};
            socket.emit('add message', data, function (result) {
                if (!result) {
                    alert('Ваше сообщение не доставлено')
                } else {
                    $scope.messageSended = false;
                    $scope.message = null;
                }
            });
        }
    };
//Функциии выбора и отмены опций редаткирования и комментирования сообщений
    $scope.checkEdit = function (message) {
        $scope.commentsMessage = null;
        $scope.editMessage = message;

    };

    $scope.uncheckEdit = function () {
        $scope.editMessage = null;
    };

    $scope.checkComment = function (message) {
        $scope.editMessage = null;
        $scope.commentsMessage = message;
    };

    $scope.uncheckComment = function () {
        $scope.commentsMessage = null;
    };
//Функция показа комментариев
    $scope.showTree = function (message) {
        if (message.nodes) {//если ветка уже была открыта, удаляем её и отписываемся от событий её обновления
            angular.forEach(message.nodes,function (item) {
                socket.off('update comment'+item.id);
            });
            socket.off("add comment to" + message.id);
            message.nodes = null;
        } else {//Получем дочерние сообщения для родителя
            messageService.getChildMessages(message.id)
                .then(
                    function (res) {
                        message.nodes = res.data.list;
                        angular.forEach(message.nodes,function (item) {
                            socket.on('update comment'+item.id,function (result) {//подписываемся на обновления данной ветки
                                var oldMessage = _.findWhere(message.nodes, {id: result.id});
                                var index = message.nodes.indexOf(oldMessage);
                                if (oldMessage.nodes) {
                                    result.nodes = oldMessage.nodes;//копируем открытые ветки для данного сообщения в новый объект
                                }
                                message.nodes.splice(index, 1, result);//заменяем объект
                            });
                        });
                        socket.on("add comment to" + message.id, function (newComment) {//вешаем обработчик для входящих комментариев с указанием id
                            message.nodes.push(newComment);
                        })
                    },
                    function (err) {
                        console.log(err);
                    }
                );

        }

    }
});
