var express = require('express');
var router = express.Router();
var path = require('path');
var  Message = require('../models').Message;

//Роут для выхода пользователя из системы
router.get('/logout',function (req, res) {
    if(req.isAuthenticated()){
        req.logout();
        res.end('logout');
    }else res.end('user not login');
});

//Обработчик всех запросов сообщений, возвращает два объекта - сам список и общее кол-во сообщений
function getMessageHandler(res) {
    return function (result) {
        if(result.rows.length<1){
            var data  = {list:[],count:0};
            res.json(data);
        }else {
            var messages = [];
            result.rows.forEach(function (message,i) {
                var modify_message = message;
                modify_message.author = JSON.parse(modify_message.author);
                messages.push(modify_message);
                if(i==result.rows.length-1){
                    res.json({list:messages,count:result.count});
                }
            });
        }
    }
}

//Обработчик ошибок для промисов sequelize
function errorHandler(next) {
    return function(err) {
        next(err);
    }
}

//Загрузка первых 10 сообщений при старте приложения
router.get('/getFirstMessages',function (req, res, next) {
   Message.findAndCountAll({
       where:{parentID:null},
       limit:10,
       offset:0,
       order:[['createdAt','DESC']]
   }).then(getMessageHandler(res),errorHandler(next));
});

//Возвращает список комментариев конкретного поста
router.get('/childMessages/:parent_id',function (req, res,next) {
    Message.findAndCountAll({where:{parentID:req.params.parent_id}})
        .then(getMessageHandler(res),errorHandler(next));
});

//Постраничная подгрузка страницы для infinite scroll
router.get('/load/:page',function (req, res,next) {
    var page = parseInt(req.params.page);
    var offset = page*10;
    Message.findAndCountAll({
        where:{parentID:null},
        limit:10,
        offset:offset,
        order:[['createdAt','DESC']]
    })
        .then(getMessageHandler(res),errorHandler(next));
});

//Возвращает все сообщения главной ветки
router.get('/allMessages',function (req, res,next) {
    Message.findAndCountAll({
        where:{parentID:null},
        order:[['createdAt','DESC']]
    })
        .then(getMessageHandler(res),errorHandler(next));
});



module.exports = router;


