var express = require('express');
var router = express.Router();
var path = require('path');

//Роут для индекса
router.get('/', function(req, res, next) {
    res.render('views/index',{user:req.user});
});

//Роут страницы авторизации
router.get('/sign_in', function(req, res, next) {
    if(req.isAuthenticated()){
        res.redirect('/');//если запрос аутентифицирован, перенаправляем пользователя на индекс
    }else {
        res.render('views/sign_in');
    }

});


module.exports = router;
