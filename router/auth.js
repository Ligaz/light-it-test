var express = require('express');
var router = express.Router();
var path = require('path');
var passport = require('passport');
//Авторизация ВК
router.get('/vk',
    passport.authenticate('vk', {
        scope: ['friends']
    }),
    function (req, res) {
    });
router.get('/vk/callback',
    passport.authenticate('vk', {
        failureRedirect: '/error'
    }),
    function (req, res) {
        res.redirect('/');
    });
//Facebook
router.get('/facebook',
    passport.authenticate('facebook')
);
router.get('/facebook/callback',
    passport.authenticate('facebook', {
        failureRedirect: '/error' }),
    function (req, res) {
        res.redirect('/');
    }
);
//Google
router.get('/google',
    passport.authenticate('google',{ scope: ['profile'] })
);
router.get('/google/callback',
    passport.authenticate('google', {
        failureRedirect: '/error' }),
    function (req, res) {
        res.redirect('/');
    }
);
module.exports = router;