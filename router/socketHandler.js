var express = require('express');
    router = express.Router(),
    path = require('path'),
    Message = require('../models').Message;

//Создаем модуль для обработки сокетов
var SocketsHandler = function (io) {
    router.io = (io);
    router.io.on('connection', function(socket){
        console.log('a user connected');
        //Добавлено сообщение
        socket.on('add message',function (data,callback) {
           var message = {
               text:data.message,
               author:JSON.stringify(data.user),
               checked:false,
               parentID:data.parentID?data.parentID:null
           };
            Message.create(message)//создаем сообщение
                .then(function (res) {
                    var mod_message = res.dataValues;
                    mod_message.author = JSON.parse(mod_message.author);
                    if(data.parentID){//этот блок отрабатывает если у сообщения есть родитель, т.е. это комментарий
                        Message.findOne({where:{id:data.parentID}})
                            .then(
                                function (item) {
                                    item.update({checked:true});//изменяем статус родителю
                                    item.author = JSON.parse(item.author);
                                    if(item.parentID){//если родитель тоже комментарий, эмитируем событие обовления с id для всех, кто подписан на него
                                        io.emit('update comment'+item.id,item);
                                    }else {
                                        io.emit('update message',item);//если это сообщение
                                    }
                                    io.emit('add comment to'+item.id,mod_message);//отдаем комментарий тем, кто на него подписан по id
                                }
                            )
                    }else {
                        io.emit('new message',mod_message);//если это сообщение главной ветки
                    }
                    callback(true); //уведомляем емитента о том, что его сообщение доставлено
                },function (err) {
                    callback(false);
                });
        });
        //Обновление сообщения
        socket.on('rewrite',function (data, callback) {
           Message.findOne({where:{id:data.editID}})
               .then(
                   function (message) {
                       message.update({text:data.message});
                       message.author = JSON.parse(message.author);
                       if(message.parentID){
                           io.emit('update comment'+message.id,message);
                       }else {
                           io.emit('update message',message);
                       }
                       callback(true);
                   },
                   function (err) {
                       console.log(err);
                       callback(false);
                   }
               )
        });
        socket.on('disconnect', function(){
            console.log('user disconnected');
        });
    });
    return router;

};

module.exports = SocketsHandler;
