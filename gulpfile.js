var gulp = require('gulp'), // Подключаем Gulp
    sass = require('gulp-sass'), //Подключаем Sass пакет,
    browserSync = require('browser-sync'), // Подключаем Browser Sync
    concat = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
    uglify = require('gulp-uglifyjs'), // Подключаем gulp-uglifyjs (для сжатия JS)
    cssnano = require('gulp-cssnano'), // Подключаем пакет для минификации CSS
    rename = require('gulp-rename'), // Подключаем библиотеку для переименования файлов
    del = require('del'), // Подключаем библиотеку для удаления файлов и папок
    imagemin = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
    pngquant = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
    cache = require('gulp-cache'), // Подключаем библиотеку кеширования
    autoprefixer = require('gulp-autoprefixer'),// Подключаем библиотеку для автоматического добавления префиксов
    nodemon = require('gulp-nodemon'),//Модуль для запуска сервера node.js
    concatCss = require('gulp-concat-css');//Модуль для конкатенации сss файлов

gulp.task('templates', function buildHTML() {
    return gulp.src('dev/templates/**/*.html')
        .pipe(gulp.dest('dist/templates'));//выгружаем в продакшн
});

gulp.task('sass', function () { // Создаем таск Sass
    return gulp.src('dev/sass/**/*.sass') // Берем источник
        .pipe(sass()) // Преобразуем Sass в CSS посредством gulp-sass
        .pipe(autoprefixer(['last 2 version', 'safari 5', 'ie 6', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'], {cascade: true})) // Создаем префиксы
        .pipe(concat('custom.css'))
        .pipe(cssnano())
        .pipe(concatCss("custom.min.css"))
        .pipe(gulp.dest('dist/css'));
});
gulp.task('css-libs', function () {
    return gulp.src('dev/css/*.css') // Выбираем файл для минификации
        .pipe(cssnano()) // Сжимаем
        .pipe(concatCss("libs.min.css"))
        .pipe(gulp.dest('dist/css')); // Выгружаем в папку dist/css
});
gulp.task('fonts', function () {
    return gulp.src('dev/fonts/*.*') // Переносим шрифты в продакшн
        .pipe(gulp.dest('dist/fonts')); // Выгружаем в папку dist/css
});
gulp.task('browser-sync', function () { // Создаем таск browser-sync
    browserSync({ // Выполняем browserSync
        proxy:"localhost:3005",
        notify: false // Отключаем уведомления
    });
});

gulp.task('def-scripts', function () {
    return gulp.src([ // Берем все необходимые библиотеки
        'bower_components/jquery/dist/jquery.js',
        'bower_components/angular/angular.min.js',
        'bower_components/lodash/dist/lodash.min.js',
        'bower_components/socket.io-client/dist/socket.io.js',
        'bower_components/socket.io-client/dist/socket.io.slim.js',
        'bower_components/angular-animate/angular-animate.min.js',
        'bower_components/angular-route/angular-route.min.js',
        'bower_components/angular-route-segment/build/angular-route-segment.js',
        'bower_components/ngstorage/ngStorage.min.js',
        'bower_components/angular-sanitize/angular-sanitize.js',
        'bower_components/angular-aria/angular-aria.min.js',
        'bower_components/angular-material/angular-material.min.js'
    ])
        .pipe(concat('libs.min.js')) // Собираем их в кучу в новом файле libs.min.js
        .pipe(uglify({mangle:false})) // Сжимаем JS файл
        .pipe(gulp.dest('dist/js')); // Выгружаем в папку dist/js
});

gulp.task('my-scripts', function () {
    return gulp.src('dev/js/**/*.js')
        .pipe(concat('app.min.js')) // Собираем их в кучу в новом файле app.min.js
        .pipe(uglify({mangle:false})) // Сжимаем JS файл
        .pipe(gulp.dest('dist/js')); // Выгружаем в папку dist/js
});
gulp.task('scripts',['def-scripts','my-scripts']);

gulp.task('start-server', function () {
    nodemon({
        script: 'app.js',
        ignore:['dev/*','dist/*']
    })
});

gulp.task('clean', function () {
    return del.sync('dist'); // Удаляем папку dist перед сборкой
});


gulp.task('build',['clean','fonts','scripts','sass','css-libs','templates','browser-sync'],function () {
    console.log('build success');
});

gulp.task('clear', function (callback) {
    return cache.clearAll();
});

gulp.task('watcher',function () {
    gulp.watch('dev/sass/**/*.sass',['sass',browserSync.reload]);
    gulp.watch('dev/js/**/*.js',['my-scripts',browserSync.reload]);
    gulp.watch('dev/templates/**/*.html',['templates',browserSync.reload]);
});


gulp.task('default',['start-server','build','watcher']);
